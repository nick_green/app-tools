/**
 * @param {number} x 
 * @param {number} y 
 */
export const add = (x, y) => x + y;