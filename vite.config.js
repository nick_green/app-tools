import path from 'path';

import { defineConfig } from 'vite';

// https://vitejs.dev/config
export default defineConfig({
    build: {
        outDir: 'build',
        sourcemap: true,
        lib: {
            entry: path.resolve(__dirname, 'src/index.js'),
            name: 'app-utils'
            // fileName: format => `my-lib.${format}.js`
        }
        // rollupOptions: {
        //     // make sure to externalize deps that shouldn't be bundled
        //     // into your library
        //     external: ['vue'],
        //     output: {
        //         // Provide global variables to use in the UMD build
        //         // for externalized deps
        //         globals: {
        //             vue: 'Vue'
        //         }
        //     }
        // }
    }
});
